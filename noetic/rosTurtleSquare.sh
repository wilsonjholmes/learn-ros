#!/bin/bash

rosservice call /reset
rosservice call /turtle1/set_pen "{r: 0, g: 255, b: 0, width: 10, 'off': 1}"
rosservice call /turtle1/teleport_absolute 5 5 0
rosservice call /turtle1/set_pen "{r: 0, g: 255, b: 0, width: 10, 'off': 0}"
rosservice call /turtle1/teleport_absolute 5 10 0
rosservice call /turtle1/teleport_absolute 10 10 0
rosservice call /turtle1/teleport_absolute 10 5 0
rosservice call /turtle1/teleport_absolute 5 5 0
