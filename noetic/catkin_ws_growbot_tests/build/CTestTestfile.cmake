# CMake generated Testfile for 
# Source directory: /home/wilson/git/learn-ros/noetic/catkin_ws_growbot_tests/src
# Build directory: /home/wilson/git/learn-ros/noetic/catkin_ws_growbot_tests/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("urdf_sim_tutorial")
subdirs("urdf_tutorial")
