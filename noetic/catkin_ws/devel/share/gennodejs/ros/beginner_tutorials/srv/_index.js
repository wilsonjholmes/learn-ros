
"use strict";

let MultiplyTwoInts = require('./MultiplyTwoInts.js')
let AddTwoInts = require('./AddTwoInts.js')

module.exports = {
  MultiplyTwoInts: MultiplyTwoInts,
  AddTwoInts: AddTwoInts,
};
