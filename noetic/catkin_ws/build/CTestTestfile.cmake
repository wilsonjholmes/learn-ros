# CMake generated Testfile for 
# Source directory: /home/wilson/git/learn-ros/noetic/catkin_ws/src
# Build directory: /home/wilson/git/learn-ros/noetic/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("hello-world")
subdirs("turtlebot3_simulations/turtlebot3_simulations")
subdirs("beginner_tutorials")
subdirs("turtlebot3_simulations/turtlebot3_fake")
subdirs("turtlebot3_simulations/turtlebot3_gazebo")
